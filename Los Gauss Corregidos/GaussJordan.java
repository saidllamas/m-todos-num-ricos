import java.util.*;
import javax.swing.*;

public class GaussJordan {

   static String log = "";

    static void muestramatriz(float matriz[][], int var) {
         log +="\n";
        for (int x = 0; x < var; x++) {
            for (int y = 0; y < (var + 1); y++) {
                log += " " + matriz[x][y] + " |";
                
            }
            log += "\n";
        }

    }

    static void pivote(float matriz[][], int piv, int var) {
        float temp = 0;
        temp = matriz[piv][piv];
        for (int y = 0; y < (var + 1); y++) {

            matriz[piv][y] = matriz[piv][y] / temp;
        }
    }

    static void hacerceros(float matriz[][], int piv, int var) {
        for (int x = 0; x < var; x++) {
            if (x != piv) {
                float c = matriz[x][piv];
                for (int z = 0; z < (var + 1); z++) {
                    matriz[x][z] = ((-1 * c) * matriz[piv][z]) + matriz[x][z];
                }
            }
        }
    }

    public static void main(String args[]) {
         JOptionPane.showMessageDialog(null, "ˇBienvenido! Gauss-Jordan");
        Scanner leer = new Scanner(System.in);
        int var = 0, piv = 0;
        float matriz[][];
        String ax = JOptionPane.showInputDialog("Ingresa numero de variables de tu sistema: ");
        var = Integer.parseInt(ax);
        matriz = new float[var][var + 1];
        for (int x = 0; x < var; x++) {
            for (int y = 0; y < (var + 1); y++) {
                String val = JOptionPane.showInputDialog("Ingresa la constante de la posicion: A[" + (x + 1) + "][" + (y + 1) + "]");
                matriz[x][y] = Float.parseFloat(val);
            }

        }

        for (int a = 0; a < var; a++) {
            pivote(matriz, piv, var);

            log += "\tRenglon " + (a + 1) + " entre el pivote";
            muestramatriz(matriz, var);

            log +="\n";

            hacerceros(matriz, piv, var);
            log += "\tHaciendo ceros";
            
            muestramatriz(matriz, var);
            log +="\n";
            piv++;
        }
        
        for (int x = 0; x < var; x++) log += "La variable X" + (x + 1) + " es: " + matriz[x][var]+", ";
        
        
         JOptionPane.showMessageDialog(null, log);

    }
}