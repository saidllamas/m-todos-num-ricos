import javax.swing.JOptionPane;

public class GaussSeidel {

   public static void main(String[] args) {
   
      int n,k,i,j,h,band,siga,miter;
      double m[][], r[], x[], y[], error[], cont[], suma,l,tol;
      
      n=Integer.parseInt(JOptionPane.showInputDialog("Ingresa el numero de incognitas"));
      tol=Double.parseDouble(JOptionPane.showInputDialog("Ingresa la tolerancia/error"));
      miter=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero maximo de iteraciones"));
      
      m= new double [n][n];
      r= new double [n];
      x= new double [n]; 
      y=new double [n];
      cont= new double [n]; 
      error= new double [n];
      
      for(i=0;i<=n-1;i++){
         k=i+1;
         r[i]=Double.parseDouble(JOptionPane.showInputDialog("Ingresa el elemento "+k+" de la matriz de soluciones"));
         x[i]=Double.parseDouble(JOptionPane.showInputDialog("Ingresa el valor en el cual se desee comenzar a evaluar x"+k));
         y[i]=0;
         for(j=0;j<=n-1;j++){
            h=j+1;
            m[i][j]=Double.parseDouble(JOptionPane.showInputDialog("Ingresa el elemento"+k+h+" de la matriz de coeficientes"));
         }
      }
      band=0;
      for(i=0;i<n;i++){
         suma=0;
         for(j=0;j<n;j++){
            if(i!=j){
               suma=suma+m[i][j];
            }
         }
         cont[i]=suma;
         if(Math.abs(m[i][i])>cont[i]){
            band=band+1;
         }
      }
      if(band==n){
         siga=n-1;
         int iter=0;
         while(siga!=n && iter<miter){
            iter=iter+1;
            for(i=0;i<n;i++){
               l=0;
               for(j=0;j<n;j++){
                  if(i==j){
                     l=l+r[i]/m[i][j];
                  }else{
                     l=l-((m[i][j]*x[j])/m[i][i]);
                  }
               }
               x[i]=l;
            }for(i=0;i<n;i++){
               error[i]=Math.abs((x[i]-y[i])/x[i])*100;
               y[i]=x[i];
            }
            siga=0;
            for(i=0;i<n;i++){
               if(error[i]<tol){
                  siga=siga+1;
               }
            }
         }
         h=0;
         for(i=0;i<n;i++){
            h=h+1;JOptionPane.showMessageDialog(null,"el valor aproximado de la incognita x"+h+" es: "+x[i]);
         }
         JOptionPane.showMessageDialog(null,"El numero total de iteraciones fue de: "+iter);
      }else {
         JOptionPane.showMessageDialog(null,"No se ha podido resolver debido a que la matriz de coeficientes no es diagonalmente dominante");
      }
   }
}