/*
*	Jesus Said Llamas Manriquez && Jose Valdovinos Vega
*  M-11 I.S.C
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.*;

public class Menu{
	public static void main(String[] args) {
		GaussSeidel gs = new GaussSeidel();
			gs.setBounds(0, 40, 600, 600); //dimension del frame
			gs.setResizable(false); //bloqueo ajuste de tamaño
			gs.setVisible(true);
			gs.addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent e){
					System.exit(0);
		        }
		    });
	    GauusJordan gj = new GauusJordan();
			gj.setBounds(610, 40, 600, 600); //dimension del frame
			gj.setResizable(false); //bloqueo ajuste de tamaño
			gj.setVisible(true);
			gj.addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent e){
					System.exit(0);
		        }
		    });
	}//main
}