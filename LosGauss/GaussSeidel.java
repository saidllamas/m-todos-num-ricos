/*
*	Jesus Said Llamas Manriquez && Jose Valdovinos Vega
*  M-11 I.S.C
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.*;

public class GaussSeidel extends Frame implements ActionListener, ItemListener{
   
   static TextArea textLog; //Area de logs
   Button btnCalcular, btnReset; //botones a usar
   Choice numVars;
   static TextField tfValue[][];
   static int numVariables = 0;
   static double matriz[][]; //matriz con valores del sistma de ecuacion

   public GaussSeidel(){
      setLayout(null);//desactivo el gestor de layout
      
      //construyendo elementos
      btnCalcular = new Button("Calcular");
      btnReset = new Button("Nuevo Calculo");
      textLog = new TextArea();
      numVars = new Choice();


      numVars.add(" "); //default
      numVars.add("2");
      numVars.add("3");
      numVars.add("4");
      numVars.add("5");
      numVars.add("6");
      numVars.add("7");
      numVars.add("8");
      numVars.add("9");
      
      
      //ajustando elementos
      btnCalcular.setBounds(40,134,170,24);
      btnCalcular.setEnabled(false);
      btnReset.setBounds(40,170,170,24);
      btnReset.setEnabled(false);
      numVars.setBounds(40, 100, 170, 20);
      textLog.setBounds(30,350, 510,200);
      textLog.setEditable(false);
      
      //escuchadores
      btnCalcular.addActionListener(this);
      btnReset.addActionListener(this);
      numVars.addItemListener(this);
      
      //añadiendo al contenedor
      add(numVars);
      add(btnReset);
      add(btnCalcular);
      add(textLog);
      
   }//constuctor
   
   public void paint(Graphics g){
      g.setFont(new Font("Arial", 0, 25)); //fuente para el titulo
      g.drawString("Gauss Seidel",200,60);  //Titulo de app
      g.setFont(new Font("Arial", 0, 14)); //fuente para contenido
      g.drawString("Cantidad de variables: ", 45, 93);
      g.drawString("Matriz : ", 260, 93);
      
      g.setFont(new Font("Arial", 0, 27)); //fuente para eL precio
      
      g.setFont(new Font("Arial", 0, 22)); //fuente para el creador
      g.drawString("Said Llamas & Jose Valdovinos", 130, 575); //Creditos
      
   }//paint
   
   public void itemStateChanged(ItemEvent e){
      int temp, temp1;
      int incrementoX = 0, incrementoY = 0;
      if(numVars.getSelectedItem() != " "){
         btnCalcular.setEnabled(true);
         btnReset.setEnabled(true);
         int vars = Integer.parseInt(numVars.getSelectedItem());
         tfValue = new TextField[vars][vars];
         for(temp = 1; temp <= vars; temp++){
            for(temp1 = 0; temp1 < vars; temp1++){
               tfValue [temp-1][temp1] = new TextField();
               tfValue [temp-1][temp1].setBounds(260+incrementoX, 100+incrementoY, 25,20);
               add(tfValue [temp-1][temp1]);
               incrementoX += 30;
              }//end columna
              incrementoX = 0;
              incrementoY += 25;
          }//end renglon
          numVariables = Integer.parseInt(numVars.getSelectedItem())-1;
          numVars.setEnabled(false);
      }
   }//itemStateChanged
   
   public void actionPerformed(ActionEvent e){
      /*       Metodos de pago          */
      if(e.getSource() == btnCalcular){
         principal();
      }else if(e.getSource() == btnReset){
         removeAll();
         dispose();
         /*arranco la app de nuevo*/
         GaussSeidel app = new GaussSeidel();
         app.setBounds(460, 40, 600, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tamaño
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               System.exit(0);
            }
         });
      }
   }//actionPerformed
   
   public static void main(String[] args){
      GaussSeidel app = new GaussSeidel();
      app.setBounds(460, 40, 600, 600); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tamaño
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main

   /*       Operaciones       */
    static void printMatriz(int var) {
        for (int x = 0; x < var; x++) {
            for (int y = 0; y < (var + 1); y++) {
                textLog.append(" " + matriz[x][y] + " |");
            }
            textLog.append("\n");
        }//for
    }//end printMatriz

    public static boolean transformarADominante(int r, boolean[] V, int[] R){
        int n = matriz.length;
        if (r == matriz.length) {
            double[][] T = new double[n][n+1];
            for (int i = 0; i < R.length; i++) {
                for (int j = 0; j < n + 1; j++) T[i][j] = matriz[R[i]][j];
            }
            matriz = T;
            return true;
        }//if
 
        for (int i = 0; i < n; i++) {
            if (V[i]) continue;
            double sum = 0;
 
            for (int j = 0; j < n; j++) sum += Math.abs(matriz[i][j]);
 
            if (2 * Math.abs(matriz[i][r]) > sum){ //ignrar el signo
                V[i] = true;
                R[r] = i;
 
                if (transformarADominante(r + 1, V, R))
                    return true;
 
                V[i] = false;
            }//if
        }//for
        return false;
    }//end transADom
 
    public static boolean hacerDominante(){
        boolean[] revisados = new boolean[matriz.length];
        int[] renglon = new int[matriz.length];
 
        Arrays.fill(revisados, false);
 
        return transformarADominante(0, revisados, renglon);
    }///end hacerDom
 
    public static void resolver(){
        int iterations = 0;
        int n = matriz.length;
        double epsilon = 1e-15;
        double[] X = new double[n]; // aproximaciones
        double[] P = new double[n]; // temp
        Arrays.fill(X, 0);
        while (true) {
            for (int i = 0; i < n; i++){
                double sum = matriz[i][n]; // b_n
 
                for (int j = 0; j < n; j++)
                    if (j != i)
                        sum -= matriz[i][j] * X[j];
                X[i] = 1/matriz[i][i] * sum;
            }
 
            //System.out.print("X_" + iterations + " = {");
            textLog.append("X_" + iterations + " = {");
            for (int i = 0; i < n; i++) textLog.append(X[i] + " "); //System.out.print(X[i] + " ");
            //System.out.println("}");
            textLog.append("} \n");
 
            iterations++;
            if (iterations == 1) 
                continue;
 
            boolean stop = true;
            for (int i = 0; i < n && stop; i++)
                if (Math.abs(X[i] - P[i]) > epsilon)
                    stop = false;
 
            if (stop || iterations == 100) break;
            P = (double[])X.clone();
        }//while
    }//end resolver

    public static void principal() {
      boolean error = false;
        int var = 0, piv = 0;
        int temp, temp1;
        //float matriz[][];
        matriz = new double[numVariables][numVariables + 1];
        for(temp = 1; temp <= numVariables; temp++){
         if(!error){
            for(temp1 = 0; temp1 < numVariables; temp1++){
              try{
                matriz[temp-1][temp1] = Double.parseDouble(tfValue[temp-1][temp1].getText());
              } catch(Exception e){
               textLog.append("Error, no puedes dejar vacio algun cuadro y tampoco puedes introducir letras \n");
               error = true;
               break;
              }
            } 
         }//if
        }//for
        if(!error){
          if (!hacerDominante()) textLog.append("Error, el sistema de ecuacion no es diagonalmente dominante. \n");
 
          //printMatriz(matriz, numVariables);
          printMatriz(numVariables);
          resolver();
        }//hubo error en la entrada para la matriz
    }//end principal
}//class
