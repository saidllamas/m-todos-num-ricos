import java.util.*;
public class Main{
	static Scanner entrada = new Scanner(System.in);
	public static void main(String[]args){
		Calculos calcular =new Calculos();
  		int opc = 0;
  		do{
  			System.out.println("**	Jose Valdovinos Vega ~ Jesus Said Llamas Manriquez	**");
  			System.out.println("Selecione una operacion:");
  			System.out.println("1) Metodo de la Regla Falsa");
  			System.out.println("2) Metodo de Biseccion");
  			System.out.println("3) Metodo de Incrementos");
  			System.out.println("4) Metodo de la Secante");
  			System.out.println("5) Salir");
  			opc = entrada.nextInt();
  			System.out.println();
   			switch(opc){
    			case 1:
    				calcular.getReglaFalsa();
    			break;
    			case 2:
    				calcular.getBiseccion();
    			break;
    			case 3:
    				calcular.getIncrementos();
    			break;
    			case 4:
    				calcular.getSecante();
    			break;
    			case 5:
    				System.exit(0);
    			break;
   			}//switch
  		}while(opc != 5);
 	}//main
}//class