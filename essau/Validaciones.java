import java.util.*;

public class Validaciones
{
 public static int validamenu()
   {
      int num=0;
      int ban=0;
   do{
       ban=0;
       Scanner input=new Scanner(System.in);
      try{
          num=input.nextInt();
         }
         catch(Exception ex)
          {
           System.out.println("Caracter no valido.");
           ban=1;
          }
      if(num<1||num>5){if(ban!=1)System.out.println("Opcion Erronea");System.out.println("\nMenu");System.out.println("1.Secante \n2.Regla Falsa \n3.Biseccion \n4.Incrementos \n5.Salir.");}
     }while(num<1||num>5||ban!=0);
     return num;
   }//valinum
   
   public static double validacoeficientes()
   {
      double num=0;
      int ban=0;
   do{
       ban=0;
       Scanner input=new Scanner(System.in);
      try{
          num=input.nextDouble();
         }
         catch(Exception ex)
          {
           System.out.println("Caracter no valido.");
           ban=1;
          }
      }while(ban!=0);
     return num;
   }//valinum


  public static int validax1yx2()
   {
      int num=0;
      int ban=0;
   do{
       ban=0;
       Scanner input=new Scanner(System.in);
      try{
          num=input.nextInt();
         }
         catch(Exception ex)
          {
           System.out.println("Caracter no valido. Solo numeros.");
           ban=1;
          }
       }while(ban!=0);
     return num;
   }//valinum

   
  public static int validaincre()
   {
      int num=0;
      int ban=0;
   do{
       ban=0;
       Scanner input=new Scanner(System.in);
      try{
          num=input.nextInt();
         }
         catch(Exception ex)
          {
           System.out.println("Caracter no valido.");
           ban=1;
          }
      if(num<=0){if(ban!=1)System.out.println("No se aceptan numeros negativos o iguales a cero");}
     }while(num<=0 || ban!=0);
     return num;
   }//valinum
 
 
 public static int validaq()
   {
      int num=0;
      int ban=0;
   do{
       ban=0;
       Scanner input=new Scanner(System.in);
      try{
          num=input.nextInt();
         }
         catch(Exception ex)
          {
           System.out.println("Caracter no valido.");
           ban=1;
          }
      if(num<=2){if(ban!=1)System.out.println("No se aceptan numeros menores a 2");}
     }while(num<=2 || ban!=0);
     return num;
   }//valinum



} 
 
 