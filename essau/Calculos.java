import java.util.*;
import java.lang.Math;

public class Calculos{
	Scanner entrada = new Scanner(System.in);
 	
 	public double[] Polinomio(){
  		double ecuacion[]=new double[8];
  		int x[]=new int[6];
  		double funcion=0;
  		int opc=0;
  		System.out.println("Ingresa los coheficientes del polinomio.");
  		for(int i=5;i>=0;i--){
    		System.out.println(" x^"+i+" ");
    		ecuacion[i]=entrada.nextInt();
   		}//for
   		System.out.println("Ingresa el coheficiente del coseno");
   		ecuacion[6]=entrada.nextInt();
   		System.out.println("Ingresa el cociente de X dentro del coseno");
   		ecuacion[7]=entrada.nextInt();
    	System.out.print("Polinomio formado: ");
    	for(int i=5; i>=0; i--){
    		if(i==0){System.out.print(ecuacion[i]+" + ");}
     		else{ 
         		if(ecuacion[i]!=0){
              		if(ecuacion[i]==1)System.out.print("x^"+i+" + ");
              		else System.out.print(ecuacion[i]+"x^"+i+" + ");
                }//if
     		}//else
    	}//for
     	System.out.println(+ecuacion[6]+"*cos("+ecuacion[7]+"x)");
    	return ecuacion;
 	}//polinomio
 
public void getSecante(){
	double ecuacion[]=new double[6];
	int z=0;
	double fx1=0.0,fx2=0.0,x1=0.0,x2=0.0,x3=0.0,fx3=0.0,Ea=1,x3ant=0.0;
	ecuacion=Polinomio();
	System.out.println("Ingresar x1");
	x1 =  entrada.nextInt();
	System.out.println("Ingresar x2");
	x2= entrada.nextInt();
	int b=1;
	System.out.println(Math.cos(ecuacion[6]*Math.pow(x2,ecuacion[7])));
	System.out.println("x1                  x2                     f(x1)                    f(x2)          x3               f(x3)                 Ea");
	while(z!=1){ 
	fx1=0.0;fx2=0.0;fx3=0.0;
		//este for para fx1
		for(int i=5;i>=0;i--){
		if(ecuacion[i] != 0) fx1=fx1+ecuacion[i]*Math.pow(x1,i);
	}
		fx1 += ecuacion[6]*Math.cos(ecuacion[7]*x1);  
		for(int i=5;i>=0;i--){
		if(ecuacion[i]!=0)fx2=fx2+ecuacion[i]*Math.pow(x2,i);
	}
		fx2+=ecuacion[6]*Math.cos(ecuacion[7]*x2);  

		x3=((fx2*x1)-(fx1*x2))/(fx2-fx1);
		for(int i=5;i>=0;i--){
		if(ecuacion[i]!=0) fx3=fx3+ecuacion[i]*Math.pow(x3,i);
	}
		fx3+=ecuacion[6]*Math.cos(ecuacion[7]*x3);
		if(x3ant==0) Ea=1;
		else Ea = ((x3-x3ant)/x3)*100;
		x3ant = x3;
		if(Ea<0) Ea*=-1;
		if(Ea<.1) z=1;
	if(b==1){System.out.println(x1+"                 "+x2+"                "+fx1+"  "+fx2+"  "+x3+"  "+fx3+"      "+"NA");}
	else if(b==2){System.out.println(x1+"                 "+x2+"  "+fx1+"  "+fx2+"  "+x3+"  "+fx3+"      "+Ea);}
	else {System.out.println(x1+"  "+x2+"  "+fx1+"  "+fx2+"  "+x3+"  "+fx3+"  "+Ea);}
	x1 = x2;
	x2 = x3;
	b++;
	}//while
	System.out.println("Raiz: "+x3);
}//method Secante 
 
 public void getReglaFalsa()
 {
   int z=0;
   double x1=0, x2=0, xr=0, fxr=0, fx1=0, fx2=0,xrAnt=0, ea=0;
   double ecuacion[]=new double[6];
   ecuacion=Polinomio();
   System.out.println("Ingeresar X1:");
   x1=entrada.nextInt();
   System.out.println("Ingeresar X2:");
   x2=entrada.nextInt();
    
    do{
      fx1=0;
      fx2=0;
      fxr=0;
      //FX1
      for(int i=5;i>=0;i--){
         if(ecuacion[i]!=0){
            fx1=fx1+ecuacion[i]*Math.pow(x1,i);
         }
      }
      fx1+=ecuacion[6]*Math.cos(ecuacion[7]*x1); 
      //FX2
      for(int i=5;i>=0;i--){
         if(ecuacion[i]!=0){
            fx2=fx2+ecuacion[i]*Math.pow(x2,i);
         }
      }
      fx2+=ecuacion[6]*Math.cos(ecuacion[7]*x2);
      //Xr
      xr=(fx2*x1-fx1*x2)/(fx2-fx1);
      //FXr
      for(int i=5;i>=0;i--){
         if(ecuacion[i]!=0){
            fxr=fxr+ecuacion[i]*Math.pow(xr,i);
         }
      }
      fxr+=ecuacion[6]*Math.cos(ecuacion[7]*xr);
      if (fxr*fx1<0){
         x2=xr;
      }else{
         x1=xr;
      }
      System.out.println("Xr:"+xr+" Ea:"+ea);
      ea=((xr-xrAnt)/xr)*100;
      xrAnt=xr;
      if (ea<=0.5){
         z=1;
      }
   }while(z!=1);
   System.out.println(xr+"");
   System.out.println();
 } 
 
 public void getBiseccion(){    
    double ecuacion[]=new double[6];
    int z=0;
    double fx1=0.0,fx2=0.0,x1=0.0,x2=0.0,xm=0.0,fxm=0.0,Ea=0.0,xmant=0.0;
    ecuacion=Polinomio();
     System.out.println("Ingresa  x1");
     x1=entrada.nextInt();
     System.out.println("Ingresa x2");
     x2=entrada.nextInt();
      
     System.out.println("x1                      x2                         f(x1)                          f(x2)                             xm                                 f(xm)                 Ea"); 
    while(z!=1){ 
      fx1=0.0;fx2=0.0;fxm=0.0;
      //this is for fx1
      for(int i=5;i>=0;i--){if(ecuacion[i]!=0){fx1=fx1+ecuacion[i]*Math.pow(x1,i);}}
      fx1+=ecuacion[6]*Math.cos(ecuacion[7]*x1);  
      //this for fx2
      for(int i=5;i>=0;i--){if(ecuacion[i]!=0){fx2=fx2+ecuacion[i]*Math.pow(x2,i);}}
      fx2+=ecuacion[6]*Math.cos(ecuacion[7]*x2); 

      if((fx1*fx2)<0)
      {
       xm=(x1+x2)/2;      
       for(int i=5;i>=0;i--){if(ecuacion[i]!=0){fxm=fxm+ecuacion[i]*Math.pow(xm,i);}}
       fxm+=ecuacion[6]*Math.cos(ecuacion[7]*xm);


       System.out.print(x1+"                    "+x2+"                     "+fx1+"                 "+fx2+"                "+xm+"                      "+fxm);
       if(fx1*fxm>0)
       {
        x1=xm;
        fx1=fxm;
       }
       else if(fx1*fxm<0)
       {
        x2=xm;
       }
       else if(fxm*fx1==0)
       {z=1;}
       
       Ea=((xm-xmant)/xm)*100;
       if(Ea<0){Ea*=-1;}
       if(Ea<=.5){z=1;}
     }
     else
     {
      System.out.println("X1 y X2 no estan aproximados.");
      break;
     }
     xmant=xm;
    System.out.println("              "+Ea);
    }//while
   System.out.println("Raiz: "+xm);
  }//method Biseccion

 
  public void getIncrementos(){
    double ecuacion[]=new double[6];
    int z=0;
    double fx1=0.0,fx=0.0,x=0.0,inc=0.0,q=0.0,xant=0,res=0.0;
    ecuacion=Polinomio();
    System.out.println("Introducir incremento");
    System.out.println("Ingresa valor inicial");
    q = entrada.nextInt();
    int b=1;
    System.out.println("Incremento                  x                           fx");
    while(z!=1){
      fx1=0;
      for(int i=5;i>=0;i--){if(ecuacion[i]!=0){fx1=fx1+ecuacion[i]*Math.pow(x,i);}}
      fx1+=ecuacion[6]*Math.cos(ecuacion[7]*x);
      if(b==1 || b==2){System.out.println(inc+"                        "+x+"                        "+fx1);}
      else {System.out.println(inc+"                        "+x+"                       "+fx1);}
      //condiciones para trabajar el problema
      if(fx1>0){inc=inc/q;x=xant+inc;}
      else{xant=x;x=x+inc;}
      fx=fx1;//creamos una variable auxiliar para no perder la original
      if(fx<0){fx*=-1;}//obtenemos el valor absoluto =)
      if(fx<0.01){z=1;}//con esta condicion el ciclo sabra cuando parar.
      b++;
    }//while
    if(b==1 || b==2){System.out.println(inc+"                        "+x+"                        "+fx1);}
    else {System.out.println(inc+"                        "+x+"                       "+fx1);}
    System.out.println("Raiz: "+x);
  }//end increment 
}//class