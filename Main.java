import java.util.Scanner;
import java.math.BigDecimal; //usada para el tipo BigDeciamal 
import java.math.BigInteger; //usada para el tipo BigInteger
import java.math.MathContext; //usada para aumentar los decmales en divisiones

/*
*	Programa desarrollado por Jesus Said Llamas Manriquez 30/01/2017 
*	para la materia Métodos Númericos.
*	El siguiente realiza operaciones avanzadas tales como: calcular exponencial,
*	seno, coseno, tangente, factorial de un numero, raiz de un numero y logaritmo.
*	Con apoyo de la serie de Maclaurin - Taylor y sin la libre Math (Solo importo las 
*	clases que ofrecen mayor longitud en datos.)
*	Me apoye de un metodo llamao 'pow' el cual me retorna el resultado de un valor 
*	elevado a x potencia.
*	Un metodo que me sirve como menú de informacion y un metodo para cada operacion.
*	Contacto: saidllamas14@gmail.com
*	Link del repositorio: https://bitbucket.org/saidllamas/m-todos-num-ricos
*/

public class Main{

	static BigDecimal cSignificativas = new BigDecimal("0.5"); //error meta 0.5x 10 ^ (2 -2)

	public static void main(String[] args) {
		while(true) init(); //menu ciclado, lo cierro manualmente
	}//end main

	public static void init(){ //Impresiones de bienvenida - menú
		boolean operacion = true;
		char opc = ' ';
		int numero = 0;
		System.out.println();
		System.out.println("		-----------------");
		System.out.println("		|  CALCULADORA  |");
		System.out.println("		-----------------");
		System.out.println();
		System.out.println("	--- --- --- --- --- --- --- ---");
		System.out.println("	|a| |b| |c| |d| |e| |f| |g| |h|" +"     : "+cSignificativas);
		System.out.println("	--- --- --- --- --- --- --- ---");
		System.out.println("  [a] Calcular seno");
		System.out.println("  [b] Calcular coseno");
		System.out.println("  [c] Calcular tangente");
		System.out.println("  [d] Calcular exponencial");
		System.out.println("  [e] Calcular factorial");
		System.out.println("  [f] Calcular raiz");
		System.out.println("  [g] Calcular logaritmo natural");
		System.out.println("  [h] Establecer cifras significativas");
		System.out.println("  [z] Terminar programa.");
		do{
			try{
				Scanner entrada = new Scanner(System.in);
				 opc = entrada.next().charAt(0);
			}catch(Exception e){
				System.out.println("   Ocurio un error: "+e); //Imprimir algun error en tiempo de ejecucion
				operacion = false;
			}//catch
		}while(!operacion);
		System.out.println();
		if(opc == 'z'){
			System.out.println("	Fin del programa.");
			System.out.println();
			System.exit(0);
		} // end programa
		System.out.println("   Ingrese numero: ");
			do{
				try{
					Scanner e = new Scanner(System.in);
					numero = e.nextInt();
				}catch(Exception e){
					System.out.println("      Ocurrio un error con la entrada, verifica que sean solo numeros.");
				}//try-catch
			}while(numero == 0);
			switch (opc) {
				case 'a':
					System.out.println("	Resultado: "+calcularSeno(numero));
					break;
				case 'b':
					System.out.println("	Resultado: "+calcularCoseno(numero));
					break;
				case 'c':
					System.out.println("	Resultado: "+calcularTangente(numero));
					break;
				case 'd':
					System.out.println("	Resultado: "+calcularExponencial(numero));
					break;
				case 'e':
					System.out.println("	Resultado: "+calcularFactorial(numero, ""));
					break;
				case 'f':
					System.out.println("	Resultado: "+calcularRaiz(numero));
					break;
				case 'g':
					System.out.println("	Resultado: "+calcularLogaritmo(new BigDecimal(numero)));
					break;
				case 'h':
					setCifraSignificativa(numero);
					break;
			}//switch
	}//end init

	public static BigDecimal calcularSeno(int num){ //correcto
		double res = 0, resa = 0;
		BigDecimal re = new BigDecimal("0");
		int inicio = 1, i = 1, iteraciones = 0;
		double error = 0;
		do{
			BigInteger fact = calcularFactorial(i, "n");
			res += inicio * pow(num, i) / fact.doubleValue();
			inicio *= -1;
			error = ((res - resa) / res) * 100;
			if(error < 0 ) error *= -1; //cambio el signo
			resa = res;
			i+=2;
			iteraciones++;
		}while(error > cSignificativas.doubleValue() );
		re = new BigDecimal(""+res);
		System.out.println("   Total de iteraciones: "+iteraciones);
		return re;
	}//end calcularSeno

	public static BigDecimal calcularCoseno (int num){ //correcto
		double res = 0, resa = 0;
		BigDecimal re = new BigDecimal("0");
		int inicio = 1, i = 0, iteraciones = 0;
		double error = 0;
		do{
			BigInteger fact = calcularFactorial(i, "n");
			res += inicio * pow(num, i) / fact.doubleValue();
			inicio *= -1;
			error = ((res - resa) / res) * 100;
			if(error < 0 ) error *= -1; //cambio el signo
			resa = res;
			i+=2;
			iteraciones++;
		}while(error > cSignificativas.doubleValue() );
		re = new BigDecimal(""+res);
		System.out.println("   Total de iteraciones: "+iteraciones);
		return re;
	}//end CalcularCoseno

	public static BigDecimal calcularTangente(int num){ //revisando
		int iteraciones = 0;
		BigDecimal res = new BigDecimal("0.0");
		try{
			BigDecimal tempSin = calcularSeno(num);
			BigDecimal tempCos = calcularCoseno(num);
			res = tempSin.divide(tempCos, MathContext.DECIMAL128);
			iteraciones++;
		}catch(Exception e){
			System.out.println("   Ocurio un error en el metodo tangente: "+e); //Imprimir algun error en tiempo de ejecucion
		}
		System.out.println("   Total de iteraciones: "+iteraciones);
		return res;
	}//end calcularTangente

	public static BigDecimal calcularExponencial(int num){ //correcto
		int iteraciones = 0, i = 1;
		BigDecimal recuperacion = new BigDecimal("0"); //por si ocurre algun error
		BigDecimal res = new BigDecimal("0.0");
		BigDecimal r = new BigDecimal("1");
		BigDecimal tempp = new BigDecimal("0.0");
		BigDecimal ant = new BigDecimal("1");
		BigDecimal err = new BigDecimal("0.0");
		try{
			do{
				tempp = BigDecimal.valueOf(pow(num, i));
				BigInteger fact = new BigInteger("0");
				fact = calcularFactorial(i, "n");
				tempp = tempp.divide(BigDecimal.valueOf(fact.longValue()), MathContext.DECIMAL128);
				recuperacion = tempp;
				r = r.add(tempp);
				err = calcularError(new BigDecimal(""+r), new BigDecimal(""+ant));
				if(err.doubleValue() < 0) err =  err.multiply(new BigDecimal("-1")); //cambio signo por ser negativo
				ant = r;
				i++;
				iteraciones++;
			}while(err.doubleValue() > cSignificativas.doubleValue());
			res = new BigDecimal(""+r);
		}catch(Exception e){
			System.out.println("   Ocurio un error en el metodo exponencial: "+e); //Imprimir algun error en tiempo de ejecucion
		}
		System.out.println("   Total de iteraciones: "+iteraciones);
		return res;
	}//end calcularExponencial

	public static BigInteger calcularFactorial(int num, String msj){ //correcto
		int iteraciones = 0;
		BigInteger res = new BigInteger("1");
		try{
			for (int i = 1; i <= num; i++) {
				BigInteger temp = new BigInteger(String.valueOf(i));//hago esta conversion por que no se puede multiplicar un int con un bigInteger
				res = res.multiply(temp);
				iteraciones++;
			}//for	
		}catch(Exception e){
			System.out.println("   Ocurio un error: "+e); //Imprimir algun error en tiempo de ejecucion
		}finally{
			if(msj != "n") System.out.println("   Total de iteraciones: "+iteraciones +" "+msj);
		}//finally
		return res;
	}//end calcularFactorial

	public static BigDecimal calcularRaiz(int num){  //correcto
		double re = 1;
		BigDecimal res = new BigDecimal("0.0");
		int i = 0;
		do{
			re = re - ((re * re - num) / (2 * re));
			i++;
		}while(i < 30); //iteraciones
		res = new BigDecimal(""+re);
		return res;
	}//end calcularRaiz

	public static BigDecimal calcularLogaritmo(BigDecimal num) { //correcto
		long x = 1000;
		int iteraciones = 0;
		MathContext context = new MathContext( 100 );
	    if (num.equals(BigDecimal.ONE))    return BigDecimal.ZERO;

	    num = num.subtract(BigDecimal.ONE);
	    BigDecimal ret = new BigDecimal(x + 1);
	    for (long i = x; i >= 0; i--) {
	    	BigDecimal temp = new BigDecimal(i / 2 + 1).pow(2);
	        temp = temp.multiply(num, context);
	        ret = temp.divide(ret, context);
	        temp = new BigDecimal(i + 1);
	        ret = ret.add(temp, context);
	        iteraciones++;
	    }//for
	    ret = num.divide(ret, context);
	    System.out.println("   Total de iteraciones: "+iteraciones);
	    return ret;
	}//end CalcularLogaritmo

	public static void setCifraSignificativa(int c){
		int x = 1;
		double temp = 0;
		for (int i = 1; i < c; i++) {
			x *= 10;
		}//for
		temp = (0.5  / x) * 10;
		//temp *= 10;
		cSignificativas = BigDecimal.valueOf(temp);
		System.out.println("   Error aproximado calculado: "+ cSignificativas);
	}//end solicitarCifrasSignificativas

	public static BigDecimal calcularError(BigDecimal vActual, BigDecimal vAprox){
		BigDecimal res = new BigDecimal("0");
		System.out.println(""+vActual+" - "+vAprox);
		res = vActual.subtract(vAprox);
		System.out.println("= "+res);
		System.out.println(""+res +" / "+ vActual);
		res = res.divide(vActual, MathContext.DECIMAL128);
		System.out.println("= "+res);
		System.out.println("* 100");
		res = res.multiply(new BigDecimal("100"));
		System.out.println("= "+res);
		return res;
	}//end calcularError

	public static double pow(double x, int n){
		double pow = 1;
		if(x == 0) return 0;
		for(int i = 1; i <= n; i++)	pow = pow * x; 
		return pow;
	}//end pow
}//end class