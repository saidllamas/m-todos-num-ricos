public class MetodosNumericos {
    public static void main(String[] args) {
        
        Metodos m = new Metodos();
        m.metodoIncremental(-4.0, 1.0, 20);
    }
}

public class Metodos {
    
    public double funcion(double x){
        
        return (x*x*x)-5;
    }
    
    public void metodoIncremental(double x0, double delta, int iter){
        
        double y0;
        double y1;
        double x1;
        
        y0 = funcion(x0);
        
        if(y0 == 0){
        
            System.out.println("x0 es ra�z" + "\t" +x0);
        }else{
            
            x1 = x0 + delta;
            int cont = 1;
            y1 = funcion(x1);
            
            while(y1 != 0 && y0*y1 > 0 && cont < iter){
            
                x0 = x1;
                y0 = y1;
                x1 = x0 + delta;
                y1 = funcion(x1);
                cont ++;
            }
            if(y1 == 0){
                
                System.out.println("x1 es ra�z" + x1);
            }else 
                if(y0*y1 < 0){
                    
                    System.out.println("x0 y x1 son intervalo" + "\t" + x0 + "\t" + x1);
                }else{
                    
                    System.out.println("No encontr� ra�z en esas iteraciones");
                }            
        }
  
    }
}





