import java.util.Scanner;

public final class BusquedaIncremental {

    float inicial, incremento, iteracion;
    double funcion; //res de la funcion
    float valorReal[] = new float[6]; //almacena los valores de la funcion

    public BusquedaIncremental() {
         Scanner entrada = new Scanner(System.in);
        leerXanterior(entrada);
        leerIncremento(entrada);
        leerIteracion(entrada);
        armarFuncion();
        
        ejecutarBusqueda();
    }//end construct
    
    public void armarFuncion(){
      boolean confirmada = false;
      //confirmar funcion
      do{
         //armando funcion
         for(int i = 5; i >= 0; i--){
            if(i == 0){
               System.out.println("Ingrese valor independiente:");
               Scanner entrada = new Scanner(System.in);
               valorReal[i] = entrada.nextFloat();
            }else{
               System.out.println("X^"+i+" ");   
               Scanner entrada = new Scanner(System.in);
               valorReal[i] = entrada.nextFloat();
            }//else
         }//end for
         //imprimir funcion generada
         System.out.println("Funcion generada: ");
         for(int i = 5; i >= 0; i--){
            if(valorReal[i] != 0){
               if(i == 0){
                  System.out.print(valorReal[i]);
               }else if(i == 1){
                  System.out.print(valorReal[i]+"X"+" ");
               }else{
                  System.out.print(valorReal[i]+"X^"+i+" ");
               }//else
            }//si hay un valor ingresado
         }//end for
         System.out.println();
         System.out.println("�Es correecta la funcion? 1) SI  2) Ingresar de nuevo.");
         Scanner entrada = new Scanner(System.in);
         entrada.next();
         int c = entrada.nextInt();
         if(c == 1) confirmada = true;
      }while(!confirmada);
    }//end armar funcion

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        BusquedaIncremental BI = new BusquedaIncremental();
    }//end main

    public double evaluarFuncion(float x) {
      /*
        funcion =  (Math.pow(x, 5) * valorReal[6]);
        funcion += (Math.pow(x, 4) * valorReal[5]);
        funcion += (Math.pow(x, 3) * valorReal[4]);
        funcion += (Math.pow(x, 2) * valorReal[3]);
        funcion += (Math.pow(x, 1) * valorReal[2]);
        funcion += x * valorReal[1];
        funcion += valorReal[0];*/
        funcion = (Math.pow(x, 3) * 1.43);
        System.out.println(funcion);
        funcion += (Math.pow(x, 2) * 1.025);
        System.out.println(funcion);
        funcion += -3.5;
        System.out.println(funcion);
        return funcion;
    }//end evaluar funct

    public void ejecutarBusqueda() {
        boolean negativo = false;
        boolean positivo = false;
        boolean raiz = false;
        double valPositivo = 0;
        double valNegativo = 0;
        
        for(float i = inicial; i < iteracion; i += incremento){
            double valor = evaluarFuncion(i);
            if(valor==0){ 
                System.out.println(i+" es una raiz");
            } else {
                if(valor <0) {
                    negativo = true;
                    valPositivo = i;
                }//-
                
                if(valor>0){
                    positivo = true;
                    valNegativo = i;
                }//+
                
                if(i!=inicial){
                    if(negativo && positivo){
                        if(valNegativo<valPositivo) System.out.println("Raiz entre "+valNegativo+" y "+valPositivo);
                        else System.out.println("Raiz entre"+valPositivo+" y "+valNegativo);
                        
                        raiz = true;
                        negativo = positivo = false;
                    }// - +
                }//incio?
            }//no nulo
        }//for
        
        if(!raiz){
            System.out.println("No se encontro una raiz");
        }//if
    }//end ejecutarBusq
    
    public void leerXanterior(Scanner s){
        System.out.println("Insertar valor inicial:");
        try {
            inicial = Float.parseFloat(s.next());
        } catch (NumberFormatException e) {
            System.out.println("Solo se aceptan n�meros");
            leerXanterior(s);
        }//try
    }//eend leerXanterior 
    
    public void leerIncremento(Scanner s){
        System.out.println("Inserte n�mero de iteraciones:");
        try {
            iteracion = Float.parseFloat(s.next());
        } catch (NumberFormatException e) {
            System.out.println("Solo se aceptan n�meros");
            leerIncremento(s);
        }//try
    }//end leerIncrementroo
    
    public void leerIteracion(Scanner s){
        System.out.println("Inserte valor de incremento:");
        try {
            incremento = Float.parseFloat(s.next());
        } catch (NumberFormatException e) {
            System.out.println("Solo se aceptan n�meros");
            leerIteracion(s);
        }//try
    }//end leerIteraciones
}//end class
