import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Euler{

      private double x, xf, h, y;
      private int steps = 0;
      
 
      public Euler(double x0, double y0, double h, double xf){
            this.x = x0;
            this.y = y0;
            this.h = h;
            this.xf = xf;
            this.steps = 0;
      }
     
      private double availFunction(double x, double y){
         return y;
      }
     
      public double forwardStep(){
         y = y + h * availFunction(x, y);
         steps ++;
         return y;
      }
     
      public double calculateError(double x, double yaprox){
         return Math.abs(Math.pow(Math.E, x) - yaprox);
      }
     
      public static void main(String args[]){
           
            String val = JOptionPane.showInputDialog("Ingrese el valor: ");
            double xf = Double.parseDouble(val);
            String msj = "";
            double x  = 0;
            double y0 = 1;
            double h  = 0.01;
           
            Euler e = new Euler(x, y0, h, xf);
           
            while (x < xf){
                  x = x+h;
                  e.setH(h);
                  e.setX(x);
                  e.forwardStep();
 
                  if(e.getX()+h> xf) h = xf - e.getX();
                 
                 msj += "Iteracion " + e.getSteps() + ": ";
                 msj +="\n";
                 msj += "\t x=" + e.getX() +  ", Euler: " + e.getY();
                 msj +="\n";
                 msj += "\t Error = " + e.calculateError(e.getX(), e.getY());
                 msj +="\n";
                 System.out.println("Calculando...");
            }
            //JOptionPane.showMessageDialog(null, msj);
            System.out.println("Proceso terminado...");
            ShowMessage euler = new ShowMessage(msj);
            SwingUtilities.invokeLater(euler);
            
      }
      public double getX() {
            return x;
      }
      public void setX(double x) {
            this.x = x;
      }
      public double getXf() {
            return xf;
      }
      public void setXf(double xf) {
            this.xf = xf;
      }
      public double getH() {
            return h;
      }
      public void setH(double h) {
            this.h = h;
      }
      public double getY() {
            return y;
      }
      public void setY(double y) {
            this.y = y;
      }
      public int getSteps() {
            return steps;
      }
      public void setSteps(int steps) {
            this.steps = steps;
      }
}

class ShowMessage implements Runnable{

  private static JFrame frame = new JFrame("Euler");
  private static String longMessage = "";
  
   public ShowMessage(){}
   
   public ShowMessage(String content){
      longMessage = content;
      JTextArea textArea = new JTextArea(6, 25);
      textArea.setText(longMessage);
      textArea.setEditable(false);
      JScrollPane scrollPane = new JScrollPane(textArea);
      JOptionPane.showMessageDialog(frame, scrollPane);
   }

  public void run(){
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(450, 300));
    frame.pack();
    frame.setLocationRelativeTo(null);
  }
  
}