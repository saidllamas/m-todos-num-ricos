public class ProgramaSimpson {
    public static void main(String[] args) {
        double resultado = new Funcion().integral(0.0, Math.PI/2, 10);
        System.out.println("Integral: "+resultado);
        try  {
            System.in.read();
        }catch (Exception e) {  }
    }
}

abstract class Simpson {

    public double integral(double a, double b, int n){
        if(n%2==1) n++;
        double h=(b-a)/n;
        double suma=f(a)+f(b);
        for(int i=1; i<n; i+=2){
            suma+=4*f(a+i*h);
        }
        for(int i=2; i<n; i+=2){
            suma+=2*f(a+i*h);
        }
        return (suma*h/3);
    }
    abstract public double f(double x);
}

class Funcion extends Simpson{

    public double f(double x){
        return Math.cos(x);
    }
}