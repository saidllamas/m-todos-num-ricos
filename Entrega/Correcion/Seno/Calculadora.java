/*
*  Jesus Said Llamas Manriquez, numero de control 15290902
*  Jose Valdovinos Vega, numero de control 15290930
*  Ingenieria en sistemas computacionales 4�B aula M11
* */

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

import javax.swing.JOptionPane;

public class Calculadora{
		
	public static Scanner entrada= new Scanner (System.in);
	
	public static void main(String[] args){
		int opc;
		do{
			JOptionPane.showMessageDialog(null, "�Bienvenido! Calculadora");
			String val = JOptionPane.showInputDialog("1. Serie de la e^x \n" 
                    +"2. Coseno\n"
                    +"3. Seno\n" 
                    +"4. Logaritmo natural\n"
                    +"5. Tangente\n"
                    +"6. Raiz cuadrada\n"
                    +"7. Factorial\n"
                    +"8. Salir");
			
     
      opc=Integer.parseInt(val);
      switch (opc) {
         case 1:
       double n, E, Eas;
       String temp;
       double x, x1, ee=0;
       int ce;
       temp = JOptionPane.showInputDialog("�Potencia a calcular e?");
       n= Double.parseDouble(temp);
       temp = JOptionPane.showInputDialog("�Cuantas cifras significativas?");
       ce = Integer.parseInt(temp);
       E =.5 * Expo(10,(2-ce));
       x = Ex(n, ee);
       ee++;
       do{
    	   x1=x+(Ex(n, ee)/calcularFactorial(ee));
          Eas=((x1-x)/x1)*100;
          if(Eas<0)
            Eas=Eas*-1;
          x=x1;
          ee++;
         }while(Eas>=E);
       JOptionPane.showMessageDialog(null, "Potencia "+n+" con "+ce+" cifras significativas queda: "+(ce-4));  
          break;
         case 2:
            double x2;
            temp = JOptionPane.showInputDialog("Coseno. " +"Ingresa el valor de x : ");
            x2 = Double.parseDouble(temp);            
            double n2;
            temp = JOptionPane.showInputDialog("Ingrese el numero de terminos de la serie:");
            n2 = Double.parseDouble(temp); 
               
            x2=((x2*3.14)/180);
            double term;
            double sum2=term=1;
         
            for(int i=1;i<n2;i++){  
               term*=((-1)*x2*x2)/(2*i*(2*i-1));
               sum2+=term;
            } 
            JOptionPane.showMessageDialog(null, sum2);   
            break;
         case 3:
            
             double x3;
             temp = JOptionPane.showInputDialog("Seno. Ingresa el valor de x : ");
             x3 = Double.parseDouble(temp);
             double n3;
             temp = JOptionPane.showInputDialog("Ingrese el numero de terminos de la serie: ");
             n3 = Double.parseDouble(temp);
            x3=((x3*3.14)/180);
            double sum3=x3;
            double term3=x3;
            for(int i=1;i<n3;i++){
               term3*=((-1)*x3*x3)/(2*i*(2*i-1));
               sum3+=term3;
            }
            JOptionPane.showMessageDialog(null, sum3);  
            break;
         case 4:
        	 temp = JOptionPane.showInputDialog("Logaritmo natural. Ingresa el valor de x : ");
        	 BigDecimal num = new BigDecimal(temp);
        	 long xx = 1000;
     		int iteraciones = 0;
     		MathContext context = new MathContext( 100 );
     	    if (num.equals(BigDecimal.ONE))    JOptionPane.showMessageDialog(null, "0.0");

     	    num = num.subtract(BigDecimal.ONE);
     	    BigDecimal ret = new BigDecimal(xx + 1);
     	    for (long i = xx; i >= 0; i--) {
     	    	BigDecimal temp2 = new BigDecimal(i / 2 + 1).pow(2);
     	        temp2 = temp2.multiply(num, context);
     	        ret = temp2.divide(ret, context);
     	        temp2 = new BigDecimal(i + 1);
     	        ret = ret.add(temp2, context);
     	        iteraciones++;
     	    }//for
     	    ret = num.divide(ret, context);
     	   JOptionPane.showMessageDialog(null, "Total de iteraciones: "+iteraciones+"\n"+ret);
                break;
         case 5:
            double xt;
            temp = JOptionPane.showInputDialog("Ingresa el valor de x : ");
            xt = Double.parseDouble(temp);
            double nt;
            temp = JOptionPane.showInputDialog("Ingrese el numero de terminos de la serie: ");
            nt = Double.parseDouble(temp);  
            xt = ( (xt * 3.14) / 180);
            double sumt=xt;
            double termt=xt;
            for(int i=1;i<nt;i++){
               termt*=((-1)*xt*xt)/(2*i*(2*i-1));
               sumt+=termt;
            }
            double termt2;
            double sumt2=termt2=1;
            for(int i=1;i<nt;i++){  
               termt2*=((-1)*xt*xt)/(2*i*(2*i-1));
               sumt2+=termt2;
            }
               
            double tang=sumt/sumt2;
            JOptionPane.showMessageDialog(null, tang);
            break;
         case 6:
            double ar;
            temp = JOptionPane.showInputDialog("Ingresa el valor de x : ");
            ar= Double.parseDouble(temp);
            double cifrasr = 0.0; 
            double pr=1;
            double sumr;
            double Ear;
            do{
               sumr = .5 * (pr + (ar / pr));
               Ear = Math.abs((sumr-pr)/sumr*100);
               JOptionPane.showMessageDialog(null, "xi "+sumr+"\n"+"error "+Ear);
               pr = sumr;
            }while (Ear > 0.5 * (Math.pow(10,(2-cifrasr) ) ) );  
            break;
         case 7:
        	 int va = 0;
        	 temp = JOptionPane.showInputDialog("Ingresa el valor de x : ");
             va = Integer.parseInt(temp);
             JOptionPane.showMessageDialog(null, "Factorial= "+calcularFactorial(va));
        	 break;
         case 8:
            System.exit(0);
            break;
         default:
        	 JOptionPane.showMessageDialog(null, "Opcion invalida");
            Calculadora.main (null);
            break;
      }
    }while(opc!=8);

   }

     public static double Expo(double a, double b){
      int base;
       double potencia=1;
       if(b==0)
         return 1;
       else{
         for(base=1; base<=(-1*b); base++){
        	 potencia=potencia/a;
            }
          return potencia;
         }
      }
     
       public static double Ex(double a, double b){
       double potencia=1;
       int base=0;
       if(b==0)
         return 1;
       else{
         for(base=1; base<=b; base++){
            potencia=potencia*a;
         }
         return potencia;
      }
    }

       
  	public static double calcularFactorial(double n){
  		double s=1;
  		for(double i = n; i > 0; i--) s = s * i;
		return s;  
   }
}