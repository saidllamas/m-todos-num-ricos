/*
*	Jesus Said Llamas Manriquez && Jose Valdovinos Vega
*  M-11 I.S.C
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

public class GauusJordan extends Frame implements ActionListener, ItemListener{
   
   Mind cerebro = new Mind();
   static TextArea textLog; //Area de logs
   Button btnCalcular, btnReset; //botones a usar
   Choice numVars;
   static TextField tfValue[][];
   static int numVariables = 0;

   public GauusJordan(){
      setLayout(null);//desactivo el gestor de layout
      
      //construyendo elementos
      btnCalcular = new Button("Calcular");
      btnReset = new Button("Nuevo Calculo");
      textLog = new TextArea();
      numVars = new Choice();


      numVars.add(" "); //default
      numVars.add("2");
      numVars.add("3");
      numVars.add("4");
      numVars.add("5");
      numVars.add("6");
      numVars.add("7");
      numVars.add("8");
      numVars.add("9");
      
      
      //ajustando elementos
      btnCalcular.setBounds(40,134,170,24);
      btnCalcular.setEnabled(false);
      btnReset.setBounds(40,170,170,24);
      btnReset.setEnabled(false);
      numVars.setBounds(40, 100, 170, 20);
      textLog.setBounds(30,350, 510,200);
      textLog.setEditable(false);
      
      //escuchadores
      btnCalcular.addActionListener(this);
      btnReset.addActionListener(this);
      numVars.addItemListener(this);
      
      //a�adiendo al contenedor
      add(numVars);
      add(btnReset);
      add(btnCalcular);
      add(textLog);
      
   }//constuctor
   
   public void paint(Graphics g){
      g.setFont(new Font("Arial", 0, 25)); //fuente para el titulo
      g.drawString("Gauss Jordan",200,60);  //Titulo de app
      g.setFont(new Font("Arial", 0, 14)); //fuente para contenido
      g.drawString("Cantidad de variables: ", 45, 93);
      g.drawString("Matriz : ", 260, 93);
      
      g.setFont(new Font("Arial", 0, 27)); //fuente para eL precio
      
      g.setFont(new Font("Arial", 0, 22)); //fuente para el creador
      g.drawString("Said Llamas & Jose Valdovinos", 130, 575); //Creditos
      
   }//paint
   
   public void itemStateChanged(ItemEvent e){
      int temp, temp1;
      int incrementoX = 0, incrementoY = 0;
      if(numVars.getSelectedItem() != " "){
         btnCalcular.setEnabled(true);
         btnReset.setEnabled(true);
         int vars = Integer.parseInt(numVars.getSelectedItem());
         tfValue = new TextField[vars][vars];
         for(temp = 1; temp <= vars; temp++){
            for(temp1 = 0; temp1 < vars; temp1++){
               tfValue [temp-1][temp1] = new TextField();
               tfValue [temp-1][temp1].setBounds(260+incrementoX, 100+incrementoY, 25,20);
               add(tfValue [temp-1][temp1]);
               incrementoX += 30;
              }//end columna
              incrementoX = 0;
              incrementoY += 25;
          }//end renglon
          numVariables = Integer.parseInt(numVars.getSelectedItem())-1;
          numVars.setEnabled(false);
      }
   }//itemStateChanged
   
   public void actionPerformed(ActionEvent e){
      /*       Metodos de pago          */
      if(e.getSource() == btnCalcular){
         principal();
      }else if(e.getSource() == btnReset){
         removeAll();
         dispose();
         /*arranco la app de nuevo*/
         GauusJordan app = new GauusJordan();
         app.setBounds(460, 40, 600, 600); //dimension del frame
         app.setResizable(false); //bloqueo ajuste de tama�o
         app.setVisible(true);
         app.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
               System.exit(0);
            }
         });
      }
   }//actionPerformed
   
   public static void main(String[] args){
      GauusJordan app = new GauusJordan();
      app.setBounds(460, 40, 600, 600); //dimension del frame
      app.setResizable(false); //bloqueo ajuste de tama�o
      app.setVisible(true);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }//main

   /*       Operaciones       */
   static void printMatriz(float matriz[][], int var) {
        for (int x = 0; x < var; x++) {
            for (int y = 0; y < (var + 1); y++) {
                //System.out.print(" " + matriz[x][y] + " |");
                textLog.append(" " + matriz[x][y] + " |");
            }
            //System.out.println("");
            textLog.append("\n");
        }

    }

    static void pivote(float matriz[][], int piv, int var) {
        float temp = 0;
        temp = matriz[piv][piv];
        for (int y = 0; y < (var + 1); y++) matriz[piv][y] = matriz[piv][y] / temp;
    }//end pivote

    static void iterar(float matriz[][], int piv, int var) {
        for (int x = 0; x < var; x++) {
            if (x != piv) {
                float c = matriz[x][piv];
                for (int z = 0; z < (var + 1); z++) matriz[x][z] = ((-1 * c) * matriz[piv][z]) + matriz[x][z];
            }
        }
    }

    public static void principal() {
      boolean error = false;
        int var = 0, piv = 0;
        int temp, temp1;
        float matriz[][];
        matriz = new float[numVariables][numVariables + 1];
        for(temp = 1; temp <= numVariables; temp++){
         if(!error){
            for(temp1 = 0; temp1 < numVariables; temp1++){
              try{
                matriz[temp-1][temp1] = Float.parseFloat(tfValue[temp-1][temp1].getText());
              } catch(Exception e){
               textLog.append("Error, no puedes dejar vacio algun cuadro y tampoco puedes introducir letras \n");
               error = true;
               break;
              }
            } 
         }//if
        }//for

        if(!error){
           for (int a = 0; a < numVariables; a++) {
               pivote(matriz, piv, numVariables);

               textLog.append("\tRenglon " + (a + 1) + " entre el pivote \n");
               printMatriz(matriz, numVariables);

               textLog.append("\n");

               textLog.append("\tHaciendo ceros\n");
               iterar(matriz, piv, numVariables);

               printMatriz(matriz, numVariables);
               textLog.append("\n");
               piv++;
           }
           for (int x = 0; x < numVariables; x++) textLog.append("La variable X" + (x + 1) + " es: " + matriz[x][numVariables]+"\n");
        }
    }//end principal
}//class
